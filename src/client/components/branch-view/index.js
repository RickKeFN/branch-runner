import React from "react";
import { connect } from "react-redux";

import {
  selectors as branchSelectors,
} from "store/branch";

import BranchRunning from "./branch-running";
import BranchInitialize from "./branch-initialize";

const BranchView = ({
  branch,
}) => (
  <div className="BranchView">
    {branch && branch.created
      ? <BranchRunning />
      : <BranchInitialize />
    }
  </div>
);

const mapState = state => ({
  branch: branchSelectors.selected(state),
});

export default connect(mapState)(BranchView);
