import React from "react";
import { Link } from "react-router-dom";

import Header from "components/header";

import BranchStats from "./branch-stats";

import "./index.scss";

const HomePage = () =>
  <div className="HomePage">
    <Header/>
    <section className="hero is-primary is-bold">
      <div className="hero-body">
        <div className="container">
          <div className="columns is-mobile">
            <div className="column">
              <h1 className="title is-2">
                BranchRunner
              </h1>
              <h2 className="subtitle">
                Run dem branches
              </h2>
            </div>
            <div className="column is-one-quarter">
              <img className="HomePage-heroLogo" src="" title="BranchRunner" />
            </div>
          </div>
        </div>
      </div>
    </section>
    <section className="HomePage-content section">
      <BranchStats />
      <div className="container">
        <div className="container has-text-centered">
          <p>
            <Link
              to="/branches"
              className="button is-large is-primary is-outlined"
              >
              <span className="icon">
                <i className="fa fa-code-fork"></i>
              </span>
              <span>
                See All Branches &raquo;
              </span>
            </Link>
          </p>
        </div>
      </div>
    </section>
  </div>;

export default HomePage;
