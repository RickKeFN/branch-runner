
const selected = ({ branch: { selected, all } }) =>
  all.find(x => x.key == selected) || null;

const totalCount = ({ branch: { all }}) =>
  all.length;

const runningCount = ({ branch: { all }}) =>
  all
    .reduce(
      (count, b) =>
        count += b.artifacts
          ? b.artifacts.filter(a => a.running).length
          : 0,
      0
    );

const latestCount = ({ branch: { all }}) =>
  all
    .filter(b =>
      b.created > (new Date().getTime() - 1000*60*60*24)
    )
    .length;


export default {
  selected,
  totalCount,
  runningCount,
  latestCount,
};
