import { routerReducer as router } from "react-router-redux";

import { reducer as config } from "./config";
import { reducer as branch } from "./branch";

export default {
  router,
  config,
  branch,
};
